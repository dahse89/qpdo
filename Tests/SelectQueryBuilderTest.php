<?php
use QueryBuilder\Core\Builder\SelectQueryBuilder;
use QueryBuilder\Core\Entity\SelectQuery;


/**
 * Created by PhpStorm.
 * User: pdahse
 * Date: 08.12.15
 * Time: 10:43
 */
class SelectQueryBuilderTest extends PHPUnit_Framework_TestCase
{
    /** @var  SelectQuery */
    private static $query;
    /** @var  SelectQueryBuilder */
    private static $sqb;

    public static function setUpBeforeClass(){
        self::$query = new SelectQuery();
        self::$sqb = new SelectQueryBuilder(self::$query);
    }

    private static function invokePrivateSQB($methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class(self::$sqb));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs(self::$sqb, $parameters);
    }

    public function testOrderBySingleField(){
        # $qpdo->orderBy('x')
        self::$query->setOrderBy(['x']);
        $this->assertEquals("ORDER BY `x`\n",$this->invokePrivateSQB('getOrderBy'));
        # $qpdo->orderBy(['x'])
        self::$query->setOrderBy([['x']]);
        $this->assertEquals("ORDER BY `x`\n",$this->invokePrivateSQB('getOrderBy'));
    }

    public function testOrderByMultiFields(){
        # $qpdo->orderBy(['x','y'])
        self::$query->setOrderBy([['x','y']]);
        $this->assertEquals("ORDER BY `x`,`y`\n",$this->invokePrivateSQB('getOrderBy'));
        # $qpdo->orderBy(['x','y','z'])
        self::$query->setOrderBy([['x','y','z']]);
        $this->assertEquals("ORDER BY `x`,`y`,`z`\n",$this->invokePrivateSQB('getOrderBy'));
    }

    public function testOrderBySimpleDirection(){
        # $qpdo->orderBy('x','DESC')
        self::$query->setOrderBy(['x','DESC']);
        $this->assertEquals("ORDER BY `x` DESC\n",$this->invokePrivateSQB('getOrderBy'));
        self::$query->setOrderBy(['x','desc']);
        $this->assertEquals("ORDER BY `x` DESC\n",$this->invokePrivateSQB('getOrderBy'));
        self::$query->setOrderBy(['x',' desc ']);
        $this->assertEquals("ORDER BY `x` DESC\n",$this->invokePrivateSQB('getOrderBy'));
        self::$query->setOrderBy(['x','ASC']);
        $this->assertEquals("ORDER BY `x` ASC\n",$this->invokePrivateSQB('getOrderBy'));
        self::$query->setOrderBy(['x','asc']);
        $this->assertEquals("ORDER BY `x` ASC\n",$this->invokePrivateSQB('getOrderBy'));
        self::$query->setOrderBy(['x',' AsC  ']);
        $this->assertEquals("ORDER BY `x` ASC\n",$this->invokePrivateSQB('getOrderBy'));
    }

    public function testOrderByDirection(){
        # $qpdo->orderBy(['x','y'],'DESC')
        self::$query->setOrderBy([['x','y'],'DESC']);
        $this->assertEquals("ORDER BY `x`,`y` DESC\n",$this->invokePrivateSQB('getOrderBy'));
        self::$query->setOrderBy([['x','y'],'desc']);
        $this->assertEquals("ORDER BY `x`,`y` DESC\n",$this->invokePrivateSQB('getOrderBy'));
        self::$query->setOrderBy([['x','y'],' desc ']);
        $this->assertEquals("ORDER BY `x`,`y` DESC\n",$this->invokePrivateSQB('getOrderBy'));
        self::$query->setOrderBy([['x','y'],'ASC']);
        $this->assertEquals("ORDER BY `x`,`y` ASC\n",$this->invokePrivateSQB('getOrderBy'));
        self::$query->setOrderBy([['x','y'],'asc']);
        $this->assertEquals("ORDER BY `x`,`y` ASC\n",$this->invokePrivateSQB('getOrderBy'));
        self::$query->setOrderBy([['x','y'],' AsC  ']);
        $this->assertEquals("ORDER BY `x`,`y` ASC\n",$this->invokePrivateSQB('getOrderBy'));
        self::$query->setOrderBy([]);
        $this->assertEquals("",$this->invokePrivateSQB('getOrderBy'));
    }

    public function testSelectSimple(){
        self::$query->setSelect(['x','y','z']);
        $this->assertEquals("SELECT\n\tx,\n\ty,\n\tz\n",$this->invokePrivateSQB('getSelect'));

        self::$query->setSelect(['`x a`','`y b`','`z c`']);
        $this->assertEquals("SELECT\n\t`x a`,\n\t`y b`,\n\t`z c`\n",$this->invokePrivateSQB('getSelect'));
    }

    public function testSelectSimpleRename(){
        self::$query->setSelect([['x' => 'a'],'y',['z' => 'b']]);
        $this->assertEquals("SELECT\n\tx as `a`,\n\ty,\n\tz as `b`\n",$this->invokePrivateSQB('getSelect'));
    }

    public function testSelectFunc(){
        self::$query->setSelect([['count(*)' => 'c']]);
        $this->assertEquals("SELECT\n\tcount(*) as `c`\n",$this->invokePrivateSQB('getSelect'));

        self::$query->setSelect([['count(if(shop_id = 23, 1, NULL))' => 'c']]);
        $this->assertEquals("SELECT\n\tcount(if(shop_id = 23, 1, NULL)) as `c`\n",$this->invokePrivateSQB('getSelect'));
    }

    public function testSelectStar(){
        self::$query->setSelect([]);
        $this->assertEquals("SELECT\n\t*\n",$this->invokePrivateSQB('getSelect'));
    }

    public function testLimit(){
        self::$query->setLimit([1]);
        $this->assertEquals("LIMIT 1\n",$this->invokePrivateSQB('getLimit'));

        self::$query->setLimit([100,10]);
        $this->assertEquals("LIMIT 100,10\n",$this->invokePrivateSQB('getLimit'));

        self::$query->setLimit([]);
        $this->assertEquals("",$this->invokePrivateSQB('getLimit'));
    }
    
    public function testGroupBy(){
        /*
        self::$query->setGroupBy(['shop_id']);
        $this->assertEquals("GROUP BY shop_id\n",$this->invokePrivateSQB('getGroupBy'));
        self::$query->setGroupBy(['shop_id','`test`']);
        $this->assertEquals("GROUP BY shop_id,`test`\n",$this->invokePrivateSQB('getGroupBy'));
        */
    }


}