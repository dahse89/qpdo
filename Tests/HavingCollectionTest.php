<?php
use QueryBuilder\Core\Collection\HavingCollection;


/**
 * Created by PhpStorm.
 * User: pdahse
 * Date: 08.12.15
 * Time: 10:43
 */
class HavingCollectionTest extends PHPUnit_Framework_TestCase
{

    private static function invokePrivate($instance, $methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class($instance));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs($instance, $parameters);
    }

    public function testFunc()
    {
        $clause = (new HavingCollection())->having('count(*)',1)->getClauseQuery();
        $this->assertEquals(" count(*) = '1'\n",$clause);
    }

    public function testFunc2()
    {
        $clause = (new HavingCollection())->having('max(test_id)',1)->getClauseQuery();
        $this->assertEquals(" max(test_id) = '1'\n",$clause);
    }

    public function testField()
    {
        $clause = (new HavingCollection())->having('`test`',1)->getClauseQuery();
        $this->assertEquals(" `test` = '1'\n",$clause);
    }
    
    public function testOperators(){
        $clause = (new HavingCollection())->having('test',1)->getClauseQuery();
        $this->assertEquals(" test = '1'\n",$clause);
        $clause = (new HavingCollection())->having('test','=',1)->getClauseQuery();
        $this->assertEquals(" test = '1'\n",$clause);
        $clause = (new HavingCollection())->having('test','!=',1)->getClauseQuery();
        $this->assertEquals(" test != '1'\n",$clause);
        $clause = (new HavingCollection())->having('test','>',1)->getClauseQuery();
        $this->assertEquals(" test > '1'\n",$clause);
        $clause = (new HavingCollection())->having('test','<',1)->getClauseQuery();
        $this->assertEquals(" test < '1'\n",$clause);
        $clause = (new HavingCollection())->having('test','in',[1,2,3])->getClauseQuery();
        $this->assertEquals(" test IN ('1','2','3')\n",$clause);
        $clause = (new HavingCollection())->having('test','not in',[1,2,3])->getClauseQuery();
        $this->assertEquals(" test NOT IN ('1','2','3')\n",$clause);
        $clause = (new HavingCollection())->having('date','between',['2015-01-01','2015-01-02'])->getClauseQuery();
        $this->assertEquals(" date BETWEEN '2015-01-01' AND '2015-01-02'\n",$clause);
    }
    
    public function testJoiner(){
        $clause = (new HavingCollection())->having('x',1)->having('y',2)->getClauseQuery();
        $this->assertEquals(" x = '1'\nAND y = '2'\n",$clause);
        $clause = (new HavingCollection())->having('x','!=',1)->having('y','in',[3,4,5])->getClauseQuery();
        $this->assertEquals(" x != '1'\nAND y IN ('3','4','5')\n",$clause);
        $clause = (new HavingCollection())->having('x',1)->orHaving('y',2)->getClauseQuery();
        $this->assertEquals(" x = '1'\nOR y = '2'\n",$clause);
        $clause = (new HavingCollection())
            ->having('x',1)->having('y',2)
            ->orHaving('z',1)->having('a',2)
            ->getClauseQuery();
        $this->assertEquals(" x = '1'\nAND y = '2'\nOR z = '1'\nAND a = '2'\n",$clause);
        $clause = (new HavingCollection())->having(function(HavingCollection $h){
            $h->having('x',1)->orHaving('y',2);
        })->having('z',1)->getClauseQuery();
        $this->assertEquals(" ( x = '1' OR y = '2' )\nAND z = '1'\n",$clause);
    }

}