<?php

namespace QueryBuilder\Tests;

use PDO;
use PDOException;
use PHPUnit_Framework_TestCase;

abstract class QpdoTestsDatabaseTestCase extends PHPUnit_Framework_TestCase
{
    /** @var PDO  */
    public static $pdo = null;

    public static function setUpBeforeClass()
    {
        if (is_null(self::$pdo)){
            $host = 'localhost';
            $user = 'root';
            $pass = 'mysql';
            $dbName = 'test';
            $port = 3306;

            $options = [
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
            ];
            $dsn = "mysql:host=$host;dbname=$dbName;port=$port";
            self::$pdo = new PDO($dsn,$user,$pass,$options);
        }

        self::initDatabase();
    }

     /**
     * Initializes the in-memory database.
     */
    public static function initDatabase()
    {
        #self::$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);
        $query = file_get_contents(__DIR__.'/sql/setup.sql');
        try {
            self::$pdo->exec($query);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
            die();
        }

    }
}