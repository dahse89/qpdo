DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
 `group_id` int(11) NOT NULL AUTO_INCREMENT,
 `group_name` varchar(255) NOT NULL,
 PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(255) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `email` varchar(255) NOT NULL,
 `firstname` varchar(255) NOT NULL,
 `lastname` varchar(255) NOT NULL,
 `birthday` datetime NOT NULL,
 `pw_check` varchar(32) NOT NULL,
 `created_at` datetime NOT NULL,
 `last_update` datetime NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `user_group`;
CREATE TABLE `user_group` (
 `user_id` int(11) NOT NULL,
 `group_id` int(11) NOT NULL,
 PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) NOT NULL,
 `role_id` int(11) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
DROP PROCEDURE IF EXISTS InsertRandUsers;
CREATE DEFINER=`root`@`localhost` PROCEDURE InsertRandUsers(IN NumRows INT)
 BEGIN
  DECLARE i INT;
  SET i = 1;
  START TRANSACTION;
  WHILE i <= NumRows DO
   INSERT INTO test.user (email, firstname, lastname, birthday, pw_check, created_at, last_update) VALUES
    (CONCAT(SUBSTRING(MD5(UUID()) FROM 5 FOR 5),'@test.com'),
     ELT(0.5 + RAND() * 8, 'Klaus', 'Maria', 'Peter', 'Anne', 'Kevin', 'Sabrina','Jonas','Kelly'),
     ELT(0.5 + RAND() * 8, 'Meyer', 'Doe', 'Jackson', 'Jule', 'Egge', 'Flotter','Bridge','Mc Donald'),
     (NOW() - INTERVAL FLOOR((RAND() * (30000-10000+1))+10000) DAY),
     MD5(UUID()),
     (NOW() - INTERVAL FLOOR((RAND() * (30-10+1))+10) DAY),
     (NOW() - INTERVAL FLOOR((RAND() * (3-1+1))+1) DAY));
   SET i = i + 1;
  END WHILE;
  COMMIT;
 END;
DROP PROCEDURE IF EXISTS InsertRandGroups;
CREATE DEFINER=`root`@`localhost` PROCEDURE InsertRandGroups(IN NumRows INT)
 BEGIN
  DECLARE i INT;
  SET i = 1;
  START TRANSACTION;
  WHILE i <= NumRows DO
   INSERT INTO test.`group` (group_name) VALUES (CONCAT('Group ',UUID_SHORT()));
   SET i = i + 1;
  END WHILE;
  COMMIT;
 END;
DROP PROCEDURE IF EXISTS InsertRandRoles;
CREATE DEFINER=`root`@`localhost` PROCEDURE InsertRandRoles(IN NumRows INT)
 BEGIN
  DECLARE i INT;
  SET i = 1;
  START TRANSACTION;
  WHILE i <= NumRows DO
   INSERT INTO test.`role` (`name`) VALUES (CONCAT('Role ',UUID_SHORT()));
   SET i = i + 1;
  END WHILE;
  COMMIT;
 END;
call InsertRandUsers(100);
CREATE TABLE tmp LIKE `user`;
ALTER TABLE tmp ADD UNIQUE (firstname, lastname);
INSERT IGNORE INTO tmp SELECT * FROM user;
RENAME TABLE `user` TO old, tmp to user;
DROP TABLE old;
ALTER TABLE `user` DROP INDEX firstname;
call InsertRandGroups(10);
call InsertRandRoles(10);
INSERT IGNORE INTO user_group select id as user_id,(select group_id from `group` ORDER BY RAND() limit 1) as group_id from user;
INSERT IGNORE INTO user_role (user_id,role_id) select id as user_id,(select id from `role` ORDER BY RAND() limit 1) as role_id from user;