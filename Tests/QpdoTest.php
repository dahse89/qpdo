<?php

namespace QueryBuilder\Tests;

use QueryBuilder\Core\Collection\WhereCollection;
use QueryBuilder\Core\Entity\InsertQuery;
use QueryBuilder\Core\Qpdo;

class QpdoTest extends QpdoTestsDatabaseTestCase
{
    /** @var Qpdo */
    public static $qpdo;

    public static function setUpBeforeClass(){
        parent::setUpBeforeClass();
        self::$qpdo = new Qpdo(self::$pdo);
    }

    public function testSelect(){
        $db = self::$qpdo;

        // no select
        $sql = $db->from('group')->getQuery();
        $this->assertRegExp('~^select\n\t\*\nfrom~i',$sql);

        // normal select
        $query = $db->select('group_id','group_name')->from('group');
        $counter = 0;
        foreach($query->fetch() as $group){
            $this->assertArrayHasKey('group_id',$group);
            $this->assertArrayHasKey('group_name',$group);
            $this->assertNotEmpty($group['group_id']);
            $this->assertNotEmpty($group['group_name']);
            $counter++;
        }
        $this->assertEquals($counter,10);

        //rewrite names
        $query = $db->select(['group_id' => 'id'],['group_name' => 'name'])->from('group');
        $counter = 0;
        foreach($query->fetch() as $group){
            $this->assertArrayHasKey('id',$group);
            $this->assertArrayHasKey('name',$group);
            $this->assertNotEmpty($group['id']);
            $this->assertNotEmpty($group['name']);
            $counter++;
        }
        $this->assertEquals($counter,10);

        // swap query order
        $this->assertEquals(
            $db->select('group_id','group_name')->from('group')->getQuery(),
            $db->from('group')->select('group_id','group_name')->getQuery()
        );

        // select calculated
        $result = $db->select(['1 + 1' => 'result'])->getScalar('int');
        $this->assertEquals($result,2);

        //select aggregation
        $result = $db->select('count(1)')->from('group')->getScalar('int');
        $this->assertEquals($result,10);
    }

    public function testWhere(){
        $db = self::$qpdo;

        // short equals check
        $result = $db->from('group')->where('group_id',1)->count();
        $this->assertEquals(1,$result);

        // with operator equals
        $result = $db->from('group')->where('group_id','=',5)->count();
        $this->assertEquals(1,$result);

        // with operator not equals
        $result = $db->from('group')->where('group_id','!=',5)->count();
        $this->assertEquals(9,$result);

        // with operator gt
        $result = $db->from('group')->where('group_id','>',5)->count();
        $this->assertEquals(5,$result);

        // with operator equals gt
        $result = $db->from('group')->where('group_id','>=',5)->count();
        $this->assertEquals(6,$result);

        // with operator lt
        $result = $db->from('group')->where('group_id','<',5)->count();
        $this->assertEquals(4,$result);

        // with operator equals lt
        $result = $db->from('group')->where('group_id','<=',5)->count();
        $this->assertEquals(5,$result);

        // with operator like
        $result = $db->from('group')->where('group_name','like','Group%')->count();
        $this->assertEquals(10,$result);

        // with operator not like
        $result = $db->from('group')->where('group_name','not like','Group%')->count();
        $this->assertEquals(0,$result);

        // with operator in
        $result = $db->from('group')->where('group_id','in',[1,2,3,100])->count();
        $this->assertEquals(3,$result);

        // test with brackets
        $query = $db->from('group')->where('group_id',1)->where(function(WhereCollection $where){
            $where->where('group_name','!=','test');
        });
        $sql = $query->getQuery();
        $this->assertEquals(preg_replace('~[^\(]+~','',$sql),'(');
        $this->assertEquals(preg_replace('~[^\)]+~','',$sql),')');
        $this->assertEquals(1,$query->count());

        // test with multi brackets
        $query = $db->from('group')->where('group_id',1)->where(function(WhereCollection $where){
            $where->where('group_name','!=','test')->where(function(WhereCollection $where){
                $where->where("x",'in',[1,2,3])->where("y",4);
            });
        });

        $sql = $query->getQuery();
        $this->assertEquals(preg_replace('~[^\(]+~','',$sql),'(((');
        $this->assertEquals(preg_replace('~[^\)]+~','',$sql),')))');
        $query->clear();

        // test or / not or
        $query = $db->from('group')->orWhere('group_id',1);
        $this->assertFalse(1 === preg_match('~or ~i',$query->getQuery()));
        $this->assertEquals(1,$query->count());


        // test or
        $query = $db->from('group')->where('group_id',1)->orWhere('group_name','like','Group%');
        $this->assertTrue(1 === preg_match('~or ~i',$query->getQuery()));
        $this->assertEquals(10,$query->count());

        // or test closure
        $query = $db->from('group')->where('group_id',1)->orWhere(function(WhereCollection $where){
            $where->where('group_id',2)->where('group_name','like','Group%')->orWhere(function(WhereCollection $where){
                $where->where('group_id',3)->where('group_name','like','Unknown%');
            });
        });

        $this->assertEquals(2,$query->count());
    }
    
    public function testJoin(){
        $db = self::$qpdo;
        $sql = $db->from('user')->leftJoin('group','user_id')->getQuery();
        $this->assertEquals("SELECT\n\t*\nFROM `user`\nLEFT JOIN `group` USING (user_id)\n",$sql);
        
        $sql = $db->from('user')->rightJoin('group','user_id')->getQuery();
        $this->assertEquals("SELECT\n\t*\nFROM `user`\nRIGHT JOIN `group` USING (user_id)\n",$sql);
        
        $sql = $db->from('user')->innerJoin('group','user_id')->getQuery();
        $this->assertEquals("SELECT\n\t*\nFROM `user`\nINNER JOIN `group` USING (user_id)\n",$sql);
        
        $sql = $db->from('user')->leftJoin('group','user_id')->rightJoin('role','role_id')->innerJoin('test','test_id')->getQuery();
        $this->assertEquals("SELECT\n\t*\nFROM `user`\nLEFT JOIN `group` USING (user_id)\nRIGHT JOIN `role` USING (role_id)\nINNER JOIN `test` USING (test_id)\n",$sql);
        
        $sql = $db->from('user')->leftJoin('group',['user.id' => 'group.user_id'])->getQuery();
        $this->assertEquals("SELECT\n\t*\nFROM `user`\nLEFT JOIN `group` ON user.id = group.user_id\n",$sql);
    } 
    
    public function testTableAlias(){
        $db = self::$qpdo;
        $sql = $db->from(['user' => 'u'])->leftJoin(['group' => 'g'],['u.id' => 'g.user_id'])->getQuery();
        $this->assertEquals("SELECT\n\t*\nFROM `user` `u`\nLEFT JOIN `group` `g` ON u.id = g.user_id\n",$sql);
    }
    
    public function testInsertSimple(){
        $db = self::$qpdo;
        $sql = $db->insert(['a' => 1, 'b' => 2])->into('test')->getQuery();
        $this->assertEquals("INSERT INTO `test`\n( `a`,`b` )\nVALUES\n( '1','2' )\n",$sql);
    }

    public function testInsertPriorityLow(){
        $db = self::$qpdo;
        $sql = $db->insert(['a' => 1, 'b' => 2])->into('test')->priority(InsertQuery::PRIORITY_LOW)->getQuery();
        $this->assertEquals("INSERT LOW_PRIORITY INTO `test`\n( `a`,`b` )\nVALUES\n( '1','2' )\n",$sql);
    }

    public function testInsertPriorityDelayed(){
        $db = self::$qpdo;
        $sql = $db->insert(['a' => 1, 'b' => 2])->into('test')->priority(InsertQuery::PRIORITY_DELAYED)->getQuery();
        $this->assertEquals("INSERT DELAYED INTO `test`\n( `a`,`b` )\nVALUES\n( '1','2' )\n",$sql);
    }

    public function testInsertPriorityHigh(){
        $db = self::$qpdo;
        $sql = $db->insert(['a' => 1, 'b' => 2])->into('test')->priority(InsertQuery::PRIORITY_HIGH)->getQuery();
        $this->assertEquals("INSERT HIGH_PRIORITY INTO `test`\n( `a`,`b` )\nVALUES\n( '1','2' )\n",$sql);
    }

    public function testInsertIgnore(){
        $db = self::$qpdo;
        $sql = $db->insert(['a' => 1, 'b' => 2])->into('test')->ignore()->getQuery();
        $this->assertEquals("INSERT IGNORE INTO `test`\n( `a`,`b` )\nVALUES\n( '1','2' )\n",$sql);
    }
    
    public function testInsertIgnoreLowPrio(){
        $db = self::$qpdo;
        $sql = $db->insert(['a' => 1, 'b' => 2])->into('test')->ignore()->priority(InsertQuery::PRIORITY_LOW)->getQuery();
        $this->assertEquals("INSERT LOW_PRIORITY IGNORE INTO `test`\n( `a`,`b` )\nVALUES\n( '1','2' )\n",$sql);
    }
    
    public function testInsertOnDuplicated(){
        $db = self::$qpdo;
        $sql = $db->insert(['a' => 1, 'b' => 2])->into('test')->onDuplicatedKeyUpdate(['a' => 1, 'c' => 2])->getQuery();
        $this->assertEquals("INSERT INTO `test`\n( `a`,`b` )\nVALUES\n( '1','2' )\nON DUPLICATE KEY UPDATE\n\t`a` = '1',\n\t`c` = '2'",$sql);
    }
    
}
