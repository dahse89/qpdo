<?php
use QueryBuilder\Core\Builder\UpdateQueryBuilder;
use QueryBuilder\Core\Entity\UpdateQuery;


/**
 * Created by PhpStorm.
 * User: andy
 * Date: 1/22/16
 * Time: 3:09 PM
 */
class UpdateQueryBuilderTest extends PHPUnit_Framework_TestCase
{
    /** @var  UpdateQuery */
    private static $query;
    /** @var  UpdateQueryBuilderTest */
    private static $sqb;

    public static function setUpBeforeClass()
    {
        self::$query = new UpdateQuery();
        self::$sqb = new UpdateQueryBuilder(self::$query);
    }

    private static function invokePrivateSQB($methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class(self::$sqb));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs(self::$sqb, $parameters);
    }

    public function testUpdate()
    {
        self::$query->update('x');
        $this->assertEquals("UPDATE `x` SET\n", $this->invokePrivateSQB('getUpdate'));
        self::$query->update(['x']);
        $this->assertEquals("UPDATE `x` SET\n", $this->invokePrivateSQB('getUpdate'));
        self::$query->update([['x']]);
        $this->assertEquals("UPDATE `x` SET\n", $this->invokePrivateSQB('getUpdate'));
    }

    public function testSet()
    {
        self::$query->set([['x' => 'a'],['z' => 'b']]);
        $this->assertEquals("SET `x`=`a`, `z`=`b` \n", $this->invokePrivateSQB('getUpdate'));
    }

}