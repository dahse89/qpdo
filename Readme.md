# Qpdo, a simple query builder for mysql using PDO
The idea behind Qpdo is not to provide a full automated ORM system etc. It's about writing SQLs on your own without struggle around with sql syntax, escaption, formating, result reading and so on.

**The project is still in the beta phase, so if you find a bug, let me now: ph.dahse@gmail.com**

Qpdo simply wraps your preconfigurated PDO instance:
```
#!php
<?php
$pdo = new PDO(..);
$qpdo = new Qpdo($pdo);
```

For a simple connect you can also use the static `justConnect` method
```
#!php
<?php
$qpdo = Qpdo::justConnect('127.0.0.1', 'root', 'test', 'test');
// or a bit more extended
$qpdo = Qpdo::justConnect('127.0.0.1', 'root', 'test', 'test', 3306, [
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
]);
```

Once you have a Qpdo instance you can start running Querys.
## Select Queries
All parts of the query can be accessed by chainable methods
```
#!php
<?php
$query = $qpdo->select('a','b')
              ->from('c')
              ->leftJoin('d',['a.id' => 'd.a_id'])
              ->where('e',1)
              ->orWhere('f','in',[1,2,3])
              ->groupBy('g')
              ->orderBy('h')
              ->having('i')
              ->limit(10)   
              ->getQuery();
/*
SELECT
        a,
        b
FROM `c`
LEFT JOIN `d` ON a.id = d.a_id
WHERE  e = '1'
OR f IN ('1','2','3')
GROUP BY g
ORDER BY `h`
LIMIT 10
*/
```

Also short hand actions are available 
```
#!php
<?php
$query = $qpdo->find('user','id',1)->getQuery();
/*
SELECT
        *
FROM `user`
WHERE id = '1'
*/
$query = $qpdo->from('user')->orderBy('date')->getQuery();
/*
SELECT
        *
FROM `user`
ORDER BY `date`
*/
```

Nested `WHERE`and `HAVING` conditions are possible
```
#!php
<?php
use QueryBuilder\Core\Collection\WhereCollection as W;
use QueryBuilder\Core\Collection\HavingCollection as H;

$query = $qpdo->from('user')
              ->where(function(W $col){
                $col->where('a',1)->orWhere('b','<',23);
              })
              ->where(function(W $col){
                $col->where('b','!=',1)->orWhere('b','>',23);  
              })
              ->having(function(H $col){
                $col->having('a + b','<',10)->orHaving(function(H $col){
                    $col->having('a + b','>',20)->having('c','like','%test%');
                });    
              })
              ->having(function(H $col){
                  $col->having('b - a','<',10)->orHaving('b - a','>=',19);  
              }); 
              
/*
SELECT
        *
FROM `user`
WHERE  ( a = '1' OR b < '23' )
AND ( b != '1' OR b > '23' )
HAVING  ( a + b < '10' OR ( a + b > '20' AND c LIKE '%test%' ) )
OR ( b - a < '10' OR b - a >= '19' )
*/
```

Actions can be directly performed on the Query
```
#!php
<?php
$qpdo->from('test')->getArray(); // return array with all rows
// or
foreach($qpdo->from('test')->fetch() as $row){}; // generator for each result row
// or
$qpdo->select('MAX(id)')->from('test')->getScalar() // get single value

```

## Insert Queries

Insert Queries are the same simple then select queries

```
#!php
<?php
$query = $qpdo->insert(['a' => 1, 'b' => 2])
              ->priority(InsertQuery::PRIORITY_HIGH)
              ->ignore()
              ->into('test')
              ->onDuplicatedKeyUpdate(['a' => 1, 'c' => 2])
              ->getQuery();
/*
INSERT HIGH_PRIORITY IGNORE INTO `test`
( `a`,`b` )
VALUES
( '1','2' )
ON DUPLICATE KEY UPDATE
        `a` = '1',
        `c` = '2'
*/
```