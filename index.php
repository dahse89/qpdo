<?php

use QueryBuilder\Core\Entity\InsertQuery;
use QueryBuilder\Core\Qpdo;


require_once 'vendor/autoload.php';


$qpdo = Qpdo::justConnect('127.0.0.1', 'ekomi', 'ekomi', 'ekomi', 3310, [
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
]);


$query = $qpdo->insert(['a' => 1, 'b' => 2])
              ->priority(InsertQuery::PRIORITY_HIGH)
              ->ignore()
              ->into('test')
              ->onDuplicatedKeyUpdate(['a' => 1, 'c' => 2])
              ->getQuery();
echo $query;


