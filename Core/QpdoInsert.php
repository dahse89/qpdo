<?php
/**
 * Created by PhpStorm.
 * User: pdahse
 * Date: 23.12.15
 * Time: 12:28
 */

namespace QueryBuilder\Core;


use PDO;
use QueryBuilder\Core\Builder\InsertQueryBuilder;
use QueryBuilder\Core\Entity\InsertQuery;

class QpdoInsert
{

    /** @var  InsertQuery */
    private $InsertQuery;
    /** @var  PDO */
    private $pdo;

    /**
     * QpdoSelect constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->InsertQuery = new InsertQuery();
    }

    /**
     * @param array [field] $fieldValueArray
     * @return QpdoInsert
     */
    public function insert($fieldValueArray){
        $this->clear();
        $this->InsertQuery->setFields(array_keys($fieldValueArray));
        $this->InsertQuery->setValues(array_values($fieldValueArray));
        return $this;
    }

    /**
     * Values can be choose from InsertQuery constance (prefix PRIORITY_)
     * 
     * - InsertQuery::PRIORITY_LOW
     * - InsertQuery::PRIORITY_DELAYED
     * - InsertQuery::PRIORITY_HIGH
     *
     * @param $priority string (see above)
     * @return QpdoInsert
     */
    public function priority($priority){
        $this->InsertQuery->setPriority($priority);
        return $this;
    }

    /**
     * @param $table string
     * @return QpdoInsert
     */
    public function into($table){
        $this->InsertQuery->setTable($table);
        return $this;
    }

    /**
     * @return QpdoInsert
     */
    public function ignore(){
        $this->InsertQuery->ignore();
        return $this;
    }

    /**
     * @param array [field] $fieldValueArray
     * @return QpdoInsert
     */
    public function onDuplicatedKeyUpdate($fieldValueArray){
        $this->InsertQuery->setOnDuplicateKeyUpdate($fieldValueArray);
        return $this;
    }
    

    public function clear(){
        $this->InsertQuery= new InsertQuery();
    }

    /**
     * @return string
     */
    private function getInsertQuery(){
        $insertQueryBuilder = new InsertQueryBuilder($this->InsertQuery);
        return $insertQueryBuilder->getQuery();
    }

    /**
     * @return string
     */
    public function getQuery(){
        return $this->getInsertQuery();
    }
    
}