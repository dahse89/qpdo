<?php
/**
 * Created by PhpStorm.
 * User: andy
 * Date: 2/1/16
 * Time: 2:09 PM
 */

namespace QueryBuilder\Core;

use PDO;
use QueryBuilder\Core\Entity\UpdateQuery;

class QpdoUpdate
{
    /** @var  UpdateQuery */
    private $query;
    /** @var  PDO */
    private $pdo;
    
    /**
     * QpdoSelect constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->query = new UpdateQuery();
    }
    
}