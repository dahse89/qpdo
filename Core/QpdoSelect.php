<?php
/**
 * Created by PhpStorm.
 * User: pdahse
 * Date: 23.12.15
 * Time: 11:02
 */

namespace QueryBuilder\Core;


use PDO;
use QueryBuilder\Core\Builder\SelectQueryBuilder;
use QueryBuilder\Core\Entity\SelectQuery;

class QpdoSelect
{
    /** @var  SelectQuery */
    private $SelectQuery;
    /** @var  PDO */
    private $pdo;

    /**
     * QpdoSelect constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
        $this->SelectQuery = new SelectQuery();
    }

    /**
     * @param ...$fields (if first field is array this list will used for fields)
     * @return $this
     */
    public function select(...$fields){
        $this->SelectQuery->setSelect($fields);
        return $this;
    }

    /**
     * @param $limit1
     * @param null $limit2
     * @return QpdoSelect
     */
    public function limit($limit1, $limit2 = null){
        $limit = [$limit1];

        if(is_numeric($limit2)){
            $limit[] = $limit2;
        }

        $this->SelectQuery->setLimit($limit);
        return $this;
    }

    /**
     * @param $table
     * @return QpdoSelect
     */
    public function from($table){
        $alias = false;
        if(is_array($table)){
            $alias = array_values($table)[0];
            $table = array_keys($table)[0];
        }
        $this->SelectQuery->setTable($table);
        if(is_string($alias)){
            $this->SelectQuery->setTableAlias($alias);
        }
        return $this;
    }

    /**
     * @param $field
     * @param null $operator
     * @param null $value
     * @param string $joiner
     * @return QpdoSelect
     */
    public function where($field, $operator = null, $value = null, $joiner = 'and'){
        $this->SelectQuery->getWhere()->where($field, $operator, $value, $joiner);
        return $this;
    }

    /**
     * @param $field
     * @param null $operator
     * @param null $value
     * @param string $joiner
     * @return QpdoSelect
     */
    public function orWhere($field, $operator = null, $value = null, $joiner = 'or'){
        $this->SelectQuery->getWhere()->where($field, $operator, $value, $joiner);
        return $this;
    }

    /**
     * @param $field
     * @param null $operator
     * @param null $value
     * @param string $joiner
     * @return QpdoSelect
     */
    public function having($field, $operator = null, $value = null, $joiner = 'or'){
        $this->SelectQuery->getHaving()->having($field, $operator, $value, $joiner);
        return $this;
    }

    /**
     * @param $field
     * @param null $operator
     * @param null $value
     * @param string $joiner
     * @return QpdoSelect
     */
    public function orHaving($field, $operator = null, $value = null, $joiner = 'or'){
        $this->SelectQuery->getHaving()->having($field, $operator, $value, $joiner);
        return $this;
    }

    /**
     * @param $fields string|string[]
     * @param string $direction
     * @return QpdoSelect
     */
    public function orderBy($fields,$direction = ''){
        $orderBy = [$fields];
        if(!empty($direction)){
            $orderBy[] = $direction;
        }
        $this->SelectQuery->setOrderBy($orderBy);
        return $this;
    }

    /**
     * @param ...$fields
     * @return QpdoSelect
     */
    public function groupBy(...$fields){
        $this->SelectQuery->setGroupBy($fields);
        return $this;
    }

    /**
     * @param $table
     * @param $joinCondition
     * @return QpdoSelect
     */
    public function leftJoin($table,$joinCondition){
        $this->SelectQuery->addJoin(['left',$table,$joinCondition]);
        return $this;
    }

    /**
     * @param $table
     * @param $joinCondition
     * @return QpdoSelect
     */
    public function rightJoin($table,$joinCondition){
        $this->SelectQuery->addJoin(['right',$table,$joinCondition]);
        return $this;
    }

    /**
     * @param $table
     * @param $joinCondition
     * @return QpdoSelect
     */
    public function innerJoin($table,$joinCondition){
        $this->SelectQuery->addJoin(['inner',$table,$joinCondition]);
        return $this;
    }

    /**
     * @return string
     */
    private function getSelectQuery(){
        $selectQueryBuilder = new SelectQueryBuilder($this->SelectQuery);
        return $selectQueryBuilder->getQuery();
    }

    /**
     * @return mixed
     */
    public function getQuery(){
        $sql = $this->getSelectQuery();
        return $sql;
    }

    /**
     * 
     */
    public function clear(){
        $this->SelectQuery = new SelectQuery();
    }

    /**
     * @return PDO
     */
    public function getPdo(){
        return $this->pdo;
    }
    
    public function query(){
        $sql = $this->getQuery();
        $this->clear();
        return $this->getPdo()->query($sql);
    }

    /**
     * @param int $fetchStyle
     * @param int $cursorOrientation
     * @param int $offset
     * @return \Generator
     */
    public function fetch($fetchStyle = PDO::FETCH_ASSOC, $cursorOrientation = PDO::FETCH_ORI_NEXT, $offset = 0){
        $rs = $this->query();
        while($row = $rs->fetch($fetchStyle,$cursorOrientation,$offset)){
            yield $row;
        }
    }

    /**
     * @param int $fetchStyle
     * @param int $cursorOrientation
     * @param int $offset
     * @return array
     */
    public function getArray($fetchStyle = PDO::FETCH_ASSOC, $cursorOrientation = PDO::FETCH_ORI_NEXT, $offset = 0){
        $array = [];
        foreach($this->fetch($fetchStyle,$cursorOrientation,$offset) as $row){
            $array[] = $row;
        }
        return $array;
    }

    /**
     * @param int $columnIndex
     * @return array
     */
    public function getColumnArray($columnIndex = 0){
        $stmt = $this->query();
        $column = [];
        while($col = $stmt->fetchColumn($columnIndex)){
            $column[] = $col;
        }
        return $column;
    }

    /**
     * @param $columnIndex
     * @return \Generator
     */
    public function fetchColumn($columnIndex){
        $stmt = $this->query();
        yield $stmt->fetchColumn($columnIndex);
    }

    /**
     * @param string $convert
     * @return mixed
     */
    public function getScalar($convert = 'none'){
        $value = $this->getArray(PDO::FETCH_NUM)[0][0];

        $toDatetime = function($val){return new \DateTime($val); };
        $convertLookup = [
            'int' => 'intval',
            'integer' => 'intval',
            'string' => function($val){return $val."";},
            'float' => 'floatval',
            'double' => 'doubleval',
            'date' => $toDatetime,
            'datetime' => $toDatetime,
            # todo extend
        ];
        if($convert !== 'none'){
            $closure = $convertLookup[$convert];
            $value = $closure($value);
        }
        return $value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getQuery();
    }

    /**
     * @return int
     */
    public function count(){
        $sql = $this->getQuery();
        $this->clear();
        $sql = preg_replace('~^select\s.+[\s\n]from~iU',"select count(*)\nfrom",$sql);
        return (int) $this->getPdo()->query($sql)->fetchColumn(0);
    }
}