<?php
/**
 * Created by PhpStorm.
 * User: pdahse
 * Date: 04.12.15
 * Time: 16:46
 */

namespace QueryBuilder\Core\Builder;


use QueryBuilder\Core\Entity\InsertQuery;

class InsertQueryBuilder
{
    private $Query;

    /**
     * InsertQueryBuilder constructor.
     * @param InsertQuery $Query
     */
    public function __construct(InsertQuery $Query)
    {
        $this->Query = $Query;
    }

    /**
     * @return string
     */
    public function getInsert(){
        $insert = "INSERT";
        $priority = $this->Query->getPriority();
        if(!empty($priority)){
            $insert .= " ".$priority;
        }
        if($this->Query->isIgnore()){
            $insert .= " IGNORE";
        }
        return $insert." ";
    }

    /**
     * @return string
     */
    public function getInto(){
        return "INTO `".$this->Query->getTable()."`\n";
    }

    /**
     * @return string
     */
    public function getFields(){
        $fields = "( ";
        foreach($this->Query->fetchField() as $field){
            $fields .= "`$field`,";
        }
        $fields = trim($fields,",")." )\n";
        return $fields;
    }

    /**
     * @return string
     */
    public function getValues(){
        $values = "VALUES\n( ";
        foreach ($this->Query->fetchValue() as $value) {
            $values .= "'".$value."',"; # todo value should be pdo quoted
        }
        $values = trim($values,",")." )\n";
        return $values;
    }

    /**
     * @return string
     */
    public function getOnDuplicatedKeyUpdate(){
        $array = $this->Query->getOnDuplicateKeyUpdate();
        $onDuplicatedKeyUpdate = '';
        if(count($array) > 0){
            $onDuplicatedKeyUpdate = "ON DUPLICATE KEY UPDATE\n";
            foreach($array as $field => $value){
                $onDuplicatedKeyUpdate .= "\t`$field` = '$value',\n"; #todo pdo quote
            }
            $onDuplicatedKeyUpdate = rtrim($onDuplicatedKeyUpdate,",\n");
        }
        return $onDuplicatedKeyUpdate;
    }
        
    public function getQuery()
    {
        $result = $this->getInsert().
            $this->getInto().
            $this->getFields().
            $this->getValues().
            $this->getOnDuplicatedKeyUpdate()
        ;
        
        return $result;
    }
}