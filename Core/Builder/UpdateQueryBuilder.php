<?php
/**
 * Created by PhpStorm.
 * User: andy
 * Date: 1/18/16
 * Time: 3:07 PM
 */

namespace QueryBuilder\Core\Builder;


use QueryBuilder\Core\Entity\UpdateQuery;

class UpdateQueryBuilder
{
    private $query;

    /**
     * InsertQueryBuilder constructor.
     * @param InsertQuery $Query
     */
    public function __construct(UpdateQuery $Query)
    {
        $this->query = $Query;
    }
}