<?php
/**
 * Created by PhpStorm.
 * User: pdahse
 * Date: 04.12.15
 * Time: 16:46
 */

namespace QueryBuilder\Core\Builder;


use QueryBuilder\Core\Entity\SelectQuery;

class SelectQueryBuilder
{
    private $Query;

    public function __construct(SelectQuery $Query)
    {
        $this->Query = $Query;
    }

    private function getSelectFields(){
        $select = "";
        foreach($this->Query->fetchSelect() as $field){
            $name = null;
            if(is_array($field)){
                $key = array_keys($field);
                $name = $field[$key[0]];
                $field = $key[0];
            }
            $field = trim($field);

            if(is_null($name)){
                $select .= "\t$field,\n";
            }else{
                $select .= "\t$field as `$name`,\n";
            }
        }
        return rtrim($select,",\n")."\n";
    }

    private function getJoins()
    {
        # ['left','user','user_id']
        # ['left','user',['user.user_id' => 'kunden.kunden_id']]
        $joins = "";
        foreach($this->Query->fetchJoin() as $join){
            $type = strtoupper($join[0]);
            if(is_array($join[1])){
                $alias = array_values($join[1])[0];
                $_table = array_keys($join[1])[0];
                $table = "`$_table` `$alias`";
            }else{
                $table = "`$join[1]`";
            }
            
            if(is_array($join[2])){
                $cond1 = array_keys($join[2])[0];
                $cond2 = $join[2][$cond1];
                $joins .= "$type JOIN $table ON $cond1 = $cond2\n";
            }else{
                $joins .= "$type JOIN $table USING ($join[2])\n";
            }
        }
        return $joins;
    }

    private function getWhere()
    {
        $result = "";
        $where = $this->Query->getWhere()->getClauseQuery();
        if($where !== ""){
            $result = 'WHERE '.$where;
        }
        return $result;
    }

    private function getOrderBy()
    {
        $result = "";
        $orderBy = $this->Query->getOrderBy();
        $count = count($orderBy);

        if($count >  0){
            if(!is_array($orderBy[0])){
                $orderBy[0] = [$orderBy[0]];
            }

            $orderBy[0] = array_map(function($field){
                return "`".trim($field,'` ')."`";
            },$orderBy[0]);
        }

        if($count === 1){
            $result = "ORDER BY ".implode(',',$orderBy[0])."\n";
        }
        elseif($count === 2 ){
            $orderBy[1] = trim(strtoupper($orderBy[1]));
            if(!in_array($orderBy[1],['ASC','DESC'])){
                throw new \Exception('Sort direction only allows ASC or DESC');
            }
            if(count($orderBy[0]) === 0){
                throw new \Exception('Order by missing field');
            }
            $result = "ORDER BY ".implode(',',$orderBy[0])." ".$orderBy[1]."\n";
        }
        return $result;
    }

    private function getHaving()
    {
        $result = "";
        $having = $this->Query->getHaving()->getClauseQuery();
        if($having !== ""){
            $result = 'HAVING '.$having;
        }
        return $result;
    }

    private function getLimit(){
        $result = "";
        $limit = $this->Query->getLimit();
        $limitc = count($limit);
        if(is_array($limit)){
            if($limitc === 1){
                $result .= "LIMIT ".$limit[0]."\n";
            }elseif($limitc === 2){
                $result .= "LIMIT ".$limit[0].",".$limit[1]."\n";
            }
        }
        return $result;
    }

    private function getFrom()
    {
        $result = "";
        $table = $this->Query->getTable();
        if(is_string($table) && $table !== ""){
            if($this->Query->hasTableAlias()){
                $alias = $this->Query->getTableAlias();
                $result = "FROM `$table` `$alias`\n";
            }else{
                $result = "FROM `$table`\n";
            }
        }
        return $result;
    }

    private function getSelect(){
        return "SELECT\n".$this->getSelectFields();
    }
    
    private function getGroupBy(){
        $fields = $this->Query->getGroupBy();
        if(count($fields) < 1){
            return "";
        }
        return "GROUP BY ".implode($fields,',')."\n";
    }

    public function getQuery()
    {
        $result = $this->getSelect()
                . $this->getFrom()
                . $this->getJoins()
                . $this->getWhere()
                . $this->getGroupBy()
                . $this->getHaving()
                . $this->getOrderBy()
                . $this->getLimit();

        return $result;
    }
}