<?php
/**
 * Created by PhpStorm.
 * User: pdahse
 * Date: 30.11.15
 * Time: 17:12
 */

namespace QueryBuilder\Core\Collection;


use QueryBuilder\Core\Entity\NestableClause;

abstract class ClauseCollection
{
    /**
     * @var NestableClause[]
     */
    private $clauses = [];
    private $count = 0;
    private $wrappedType;
    private $wrappedTypeSingle;
    
    public function __construct($wrappedType, $wrappedTypeSingle)
    {
        $this->wrappedType = $wrappedType;
        $this->wrappedTypeSingle = $wrappedTypeSingle;
    }
    

    protected function genericOrClause($field, $operator = null, $value = null){
        return $this->genericClause($field, $operator, $value, 'or');
    }
    
    protected function addClause($clause){
        $this->clauses[] = $clause;
    }

    protected function genericClause($field, $operator = null, $value = null, $joiner = 'and'){
        $genericType = $this->wrappedTypeSingle;
        $Generic = null;

        // if short equals
        if(empty($value) && !empty($operator) && !empty($field)){
            $value = $operator;
            $Generic = new $genericType($field,$value,'=',$joiner);
        }
        // if general syntax
        elseif(!(empty($value) || empty($operator) || empty($field))){
            $Generic = new $genericType($field,$value,$operator,$joiner);
        }
        // is nested callback
        elseif(is_callable($field) && empty($operator) && empty($value)){
            /** @var NestableClause $Generic */
            $Generic = $genericType::withCallable($field,$joiner);
        }

        if(!is_null($Generic)){
            $this->addClause($Generic);
        }

        $this->increaseCounter();
        return $this;
    }
        
    protected function increaseCounter(){
        $this->count++;
    }

    protected function getArray(){
        return $this->clauses;
    }

    /**
     * @return \Generator
     */
    protected function fetch(){
        foreach($this->clauses as $clause){
            yield $clause;
        }
    }
    
    private function quotes($x){
        return "'".$x."'";
    }

    protected function fullTerm($term){
        $term[1] = strtolower($term[1]);
        if(in_array($term[1],['in','not in'])){
            $term[2] = "(".implode(",",array_map([$this,'quotes'],$term[2])).")";
        }elseif($term[1] === 'between'){
            $term[2] = implode(' AND ', array_map([$this,'quotes'],$term[2]));
        }
        
        return " " . $term[0] . " " . strtoupper($term[1]) . " " . $term[2] . "\n";
    }

    protected function closureTerm(callable $closure){
        $clazz = $this->wrappedType;
        $instance = new $clazz;
        $closure($instance);
        return " (" . preg_replace('~[\n]+~',' ',$instance->getClauseQuery()) . ")\n";
    }

    protected function concatTerm($term){
        $counter = count($term);
        $expr = "";
        if($counter === 4){
            $expr =  $this->fullTerm($term);
        }elseif($counter === 2){
            $expr = $this->closureTerm($term[0]);
        }
        return $expr;
    }

    protected function handleClauseTermConnector($first, $term){
        return $first ? "" : strtoupper($term[count($term)-1]);
    }

    protected function count(){
        return $this->count;
    }

    public function getClauseQuery(){
        $clause = "";
        $first = true;
        if($this->count() > 0){
            foreach($this->fetch() as $Where){
                $term = $Where->toArray();
                $clause .= $this->handleClauseTermConnector($first,$term);
                $first = false;
                $clause .= $this->concatTerm($term);
            }
        }
        return $clause;
    }
}