<?php
/**
 * Created by PhpStorm.
 * User: pdahse
 * Date: 30.11.15
 * Time: 17:12
 */

namespace QueryBuilder\Core\Collection;


use QueryBuilder\Core\Entity\Where;

class WhereCollection extends ClauseCollection
{
    public function __construct()
    {
        parent::__construct(self::class, Where::class);
    }

    public function where($field, $operator = null, $value = null, $joiner = 'and'){
        $this->genericClause($field, $operator, $value, $joiner);
        return $this;
    }

    public function orWhere($field, $operator = null, $value = null){
        $this->genericClause($field, $operator, $value, 'or');
        return $this;
    }
}