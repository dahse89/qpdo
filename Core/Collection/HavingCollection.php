<?php
/**
 * Created by PhpStorm.
 * User: pdahse
 * Date: 30.11.15
 * Time: 17:12
 */

namespace QueryBuilder\Core\Collection;


use QueryBuilder\Core\Entity\Having;

class HavingCollection extends ClauseCollection
{

    public function __construct()
    {
        parent::__construct(self::class, Having::class);
    }
    
    public function having($field, $operator = null, $value = null, $joiner = 'and'){
        $this->genericClause($field, $operator, $value, $joiner);
        return $this;
    }

    public function orHaving($field, $operator = null, $value = null){
        $this->genericClause($field, $operator, $value, 'or');
        return $this;
    }
}