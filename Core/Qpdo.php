<?php
namespace QueryBuilder\Core;

use PDO;

class Qpdo {
    
    private $pdo;
    /** @var  QpdoSelect */
    private $select;
    /** @var  QpdoInsert */
    private $insert;

    public function __construct(PDO $pdo){
        $this->pdo = $pdo;
        $this->select = new QpdoSelect($this->pdo);
        $this->insert = new QpdoInsert($this->pdo);
    }

    public static function justConnect($host, $user, $pass, $dbName, $port = 3306, $options = []){
        $dsn = "mysql:host=$host;dbname=$dbName;port=$port";
        $pdo = new PDO($dsn,$user,$pass,$options);
        return new self($pdo);
    }


    /**
     * @param ...$fields
     * @return QpdoSelect
     */
    public function select(...$fields){
        $this->select->clear();
        $select = $this->select; 
        return call_user_func_array([$select,'select'],$fields);
    }

    /**
     * @param $table
     * @return QpdoSelect
     */
    public function from($table){
        return $this->select()->from($table);
    }

    /**
     * @param $table
     * @param $field
     * @param $value
     * @return QpdoSelect
     */
    public function find($table,$field,$value){
        return $this->select()->from($table)->where($field,$value);
    }

    /**
     * @param array [field] $fieldValueArray
     * @return QpdoInsert
     */
    public function insert($fieldValueArray){
        return $this->insert->insert($fieldValueArray);
    }

    /**
     * @return PDO
     */
    public function getPdo(){
        return $this->pdo;
    }
}



