<?php
/**
 * Created by PhpStorm.
 * User: pdahse
 * Date: 26.11.15
 * Time: 12:42
 */

namespace QueryBuilder\Core\Entity;


class NestableClause
{
    private $field;
    private $operator;
    private $value;
    private $joiner;
    private $callable;


    /**
     * Where constructor.
     * @param $field
     * @param $operator
     * @param $value
     * @param $joiner
     */
    public function __construct($field = null, $value = null, $operator = '=', $joiner = 'and')
    {

        $this->setField($field)
             ->setOperator($operator)
             ->setValue($value)
             ->setJoiner($joiner);

    }

    public static function withCallable($callable, $joiner = 'and'){
        $self = new self();
        $self->setCallable($callable)
             ->setJoiner($joiner);
        return $self;
    }

    public function toArray(){
        if(!isset($this->callable)){
            return [$this->getField(), $this->getOperator(), $this->getValue(), $this->getJoiner()];
        }
        return [$this->getCallable(), $this->getJoiner()];
    }


    /**
     * @return mixed
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param $field
     * @return $this
     */
    public function setField($field)
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param $operator
     * @return $this
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = is_array($value) ? $value : "'$value'";
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJoiner()
    {
        return $this->joiner;
    }

    /**
     * @param $joiner
     * @return $this
     */
    public function setJoiner($joiner)
    {
        $this->joiner = $joiner;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCallable()
    {
        return $this->callable;
    }

    /**
     * @param callable $callable
     * @return $this
     */
    public function setCallable(callable $callable)
    {
        $this->callable = $callable;
        return $this;
    }

}