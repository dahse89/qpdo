<?php

namespace QueryBuilder\Core\Entity;

use QueryBuilder\Core\Collection\HavingCollection;
use QueryBuilder\Core\Collection\WhereCollection;

class SelectQuery extends Query
{
    private $select;
    private $tableAlias = false;
    /** @var WhereCollection  */
    private $where;
    private $joins = [];
    private $groupBy = [];
    /** @var HavingCollection  */
    private $having;
    private $orderBy = [];
    private $limit = [];

    public function __construct(){
        $this->where = new WhereCollection();
        $this->having = new HavingCollection();
    }
    
    /**
     * @return mixed
     */
    public function getSelect()
    {
        return $this->select;
    }

    /**
     * @param mixed $select
     */
    public function setSelect($select)
    {
        $this->select = $select;
    }

    /**
     * @param $select
     */
    public function addSelect($select){
        $this->select[] = $select;
    }

    /**
     * @return \Generator
     */
    public function fetchSelect(){
        if(!empty($this->select)){
            foreach($this->select as $select){
                yield $select;
            }
            return;
        }
        yield '*';
    }



    /**
     * @return bool
     */
    public function hasTableAlias(){
        return is_string( $this->tableAlias );
    }

    /**
     * @return string|bool
     */
    public function getTableAlias(){
        return $this->tableAlias;
    }

    /**
     * @param string $alias
     */
    public function setTableAlias($alias){
        $this->tableAlias = $alias;
    }
    

    /**
     * @return WhereCollection
     */
    public function getWhere()
    {
        return $this->where;
    }

    /**
     * @param array $where
     */
    public function setWhere($where)
    {
        $this->where = $where;
    }

    /**
     * @return HavingCollection
     */
    public function getHaving()
    {
        return $this->having;
    }

    /**
     * @param array $having
     */
    public function setHaving($having)
    {
        $this->having= $having;
    }

    /**
     * @return array
     */
    public function getJoins()
    {
        return $this->joins;
    }

    /**
     * @param array $joins
     */
    public function setJoins($joins)
    {
        $this->joins = $joins;
    }

    /**
     * @param $join
     */
    public function addJoin($join){
        $this->joins[] = $join;

    }

    /**
     * @return \Generator
     */
    public function fetchJoin(){
        foreach($this->joins as $join){
            yield $join;
        }
    }

    /**
     * @return array
     */
    public function getGroupBy()
    {
        return $this->groupBy;
    }

    /**
     * @param array $groupBy
     */
    public function setGroupBy($groupBy)
    {
        $this->groupBy = $groupBy;
    }
    
    

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
    }

    /**
     * @param $orderBy
     */
    public function addOrderBy($orderBy){
        $this->orderBy[] = $orderBy;
    }

    /**
     * @return \Generator
     */
    public function fetchOrderBy(){
        foreach($this->orderBy as $orderBy){
            yield $orderBy;
        }
    }

    /**
     * @return array
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param array $limit e.g [1] = limit 1; [1,2] = limit 1,2
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }


}