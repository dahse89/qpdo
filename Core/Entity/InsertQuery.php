<?php
/**
 * Created by PhpStorm.
 * User: pdahse
 * Date: 22.12.15
 * Time: 10:24
 */

namespace QueryBuilder\Core\Entity;


class InsertQuery extends Query
{
    const PRIORITY_LOW = 'LOW_PRIORITY';
    const PRIORITY_DELAYED = 'DELAYED';
    const PRIORITY_HIGH = 'HIGH_PRIORITY';
    
    private $fields = [];
    private $priority = '';
    private $ignore = false;
    private $values = [];
    private $onDuplicateKeyUpdate = [];

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    /**
     * @return \Generator
     */
    public function fetchField(){
        foreach($this->fields as $field){
            yield $field;
        }
    }

    /**
     * @return string
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param string $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return boolean
     */
    public function isIgnore()
    {
        return $this->ignore;
    }

    /**
     * INSERT IGNORE
     */
    public function ignore()
    {
        $this->ignore = true;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param array $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }


    /**
     * @return \Generator
     */
    public function fetchValue(){
        foreach($this->values as $value){
            yield $value;
        }
    }

    /**
     * @return array
     */
    public function getOnDuplicateKeyUpdate()
    {
        return $this->onDuplicateKeyUpdate;
    }

    /**
     * @param array $onDuplicateKeyUpdate
     */
    public function setOnDuplicateKeyUpdate($onDuplicateKeyUpdate)
    {
        $this->onDuplicateKeyUpdate = $onDuplicateKeyUpdate;
    }
    
}