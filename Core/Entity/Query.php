<?php
/**
 * Created by PhpStorm.
 * User: pdahse
 * Date: 22.12.15
 * Time: 10:24
 */

namespace QueryBuilder\Core\Entity;


class Query
{
    protected $table;


    /**
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }


    /**
     * @param mixed $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }
}